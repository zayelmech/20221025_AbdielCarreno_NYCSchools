package com.example.a20221025_abdielcarreno_nycschools.views


import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Before
import com.example.a20221025_abdielcarreno_nycschools.R

import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolFragmentTest {

    private lateinit var scenario: FragmentScenario<SchoolFragment>

//    @get:Rule
//    var hiltRule = HiltAndroidRule(this)

    @Before
     fun setup() {

        scenario =
            launchFragmentInContainer(themeResId = R.style.Theme_20221025AbdielCarrenoNYCSchools)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test
    fun findingAnSchool(){

        onView(withId(R.id.search_bar)).perform(typeText("esperanza"))
        Espresso.closeSoftKeyboard()
        onView(withText("Esperanza Preparatory Academy")).check(matches(isDisplayed()))

//        onView(withId(R.id.schools_list))
//            .check(matches( withText("Esperanza Preparatory Academy")))
//
//       assertEquals(1,1)
    }

}