package com.example.a20221025_abdielcarreno_nycschools.views

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.a20221025_abdielcarreno_nycschools.MainActivity
import com.example.a20221025_abdielcarreno_nycschools.R
import com.example.a20221025_abdielcarreno_nycschools.adapter.SchoolsAdapter.SchoolsViewHolder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get: Rule
    val activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun list_is_displayed() {

        onView(withId(R.id.schools_list)).check(matches(isDisplayed()))
    }


    @Test
    fun test_selectListItem_isDetailFragmentVisible() {
        onView(withId(R.id.schools_list))
            .perform(
                actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    1,
                    click()
                )
            )

        onView(withId(R.id.score_school_name)).check(matches(isDisplayed())) //withText("LIBERATION DIPLOMA PLUS")
    }

}