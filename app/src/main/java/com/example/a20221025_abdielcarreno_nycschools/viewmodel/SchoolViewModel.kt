package com.example.a20221025_abdielcarreno_nycschools.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20221025_abdielcarreno_nycschools.model.Schools
import com.example.a20221025_abdielcarreno_nycschools.model.Score
import com.example.a20221025_abdielcarreno_nycschools.utils.UIState
import com.imecatro.nyc_schools.network.SchoolRepository
import com.imecatro.nyc_schools.network.SchoolRepositoryImp
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

/**
 * This is my ViewModel
 * In simple terms this is the most valuable player which means that it is in charge of handle all the data
 * It get the data from the repository and then it update the data in the LiveData
 * once the data change in the LiveData, the Views can react to the changes
 * I am using Dependency injection
 *
 * */

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val schoolRepository: SchoolRepository
) : ViewModel() {

    /**
     * schools is
     */
    private val _schools: MutableLiveData<UIState> = MutableLiveData(UIState.LOADING)
    val schools: LiveData<UIState> get() = _schools

    private val _scoreData = MutableLiveData<List<Score>>()
    val scoreData: LiveData<List<Score>> get() = _scoreData


    private val compositeDisposable by lazy {
        CompositeDisposable()
    }

    fun getSchoolsList() {
        schoolRepository.getSchoolsRx()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            //.map { //change value of each item } like pass to uppercase
            //.flatMap { // combining data and merging ..}
            .subscribe(
                {
                    _schools.value = UIState.SUCCESS(it)
                },
                { _schools.value = UIState.ERROR(it) }
            )
            .also {
                compositeDisposable.add(it)
            }
    }

    fun searchSchool(keyword: String) : List<Schools>{
        val schoolsList: MutableList<Schools> = mutableListOf()
        schools.value.let { state ->

            (state as UIState.SUCCESS<List<Schools>>).response
            schoolsList.addAll(state.response)
        }

        //_schools.value = UIState.LOADING

        return schoolsList.filter { school ->
            school.schoolName.lowercase().contains(keyword)
        }
        //_schools.value = UIState.SUCCESS(newList)

    }

    fun getThisScore(dbn: String) {
        schoolRepository.getSatResultsRx(dbn)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { _scoreData.value = it },
                { _scoreData.value = emptyList() }
            )
            .also { compositeDisposable.add(it) }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}