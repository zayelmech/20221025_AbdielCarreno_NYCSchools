package com.imecatro.nyc_schools.network

import com.example.a20221025_abdielcarreno_nycschools.model.Schools
import com.example.a20221025_abdielcarreno_nycschools.model.Score
import io.reactivex.Single
import javax.inject.Inject

interface SchoolRepository {

    fun getSchoolsRx(): Single<List<Schools>>
    fun getSatResultsRx(dbn: String): Single<List<Score>>
}

class SchoolRepositoryImp @Inject constructor(
    private val schoolService: SchoolService
) : SchoolRepository {

    override fun getSchoolsRx(): Single<List<Schools>> =
        schoolService.getSchoolsRx()

    override fun getSatResultsRx(dbn: String): Single<List<Score>> =
        schoolService.getSatResultsRx(dbn)
}
