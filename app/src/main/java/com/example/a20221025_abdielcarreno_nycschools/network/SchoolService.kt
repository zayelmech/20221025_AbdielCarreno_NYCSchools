package com.imecatro.nyc_schools.network

import com.example.a20221025_abdielcarreno_nycschools.model.Schools
import com.example.a20221025_abdielcarreno_nycschools.model.Score
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolService {

    @GET(SCHOOLS)
    fun getSchoolsRx(): Single<List<Schools>>


    @GET(SCORE_ENDPOINT)
    fun getSatResultsRx(
        @Query("dbn") dbn : String
    ): Single<List<Score>>

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
        const val SCHOOLS = "s3k6-pzi2.json"
        const val SCORE_ENDPOINT = "f9bf-2cp4.json"
    }
}
