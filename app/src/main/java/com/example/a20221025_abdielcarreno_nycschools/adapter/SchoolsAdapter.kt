package com.example.a20221025_abdielcarreno_nycschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20221025_abdielcarreno_nycschools.databinding.CardItemBinding
import com.example.a20221025_abdielcarreno_nycschools.model.Schools

class SchoolsAdapter(
    private val onCardClicked: (String) -> Unit
) : RecyclerView.Adapter<SchoolsAdapter.SchoolsViewHolder>() {

    private val schoolsSet: MutableList<Schools> = mutableListOf()

    fun setData(data: List<Schools>) {
        schoolsSet.clear()
        schoolsSet.addAll(data)
        notifyDataSetChanged()
    }

    inner class SchoolsViewHolder(private val binding: CardItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(school: Schools) {
            binding.apply {
                schoolsName.text = school.schoolName
                schoolsWebsite.text = school.website
                itemView.setOnClickListener {
                    onCardClicked(school.dbn)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder =
        SchoolsViewHolder(
            CardItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false

            )
        )

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) =
        holder.bind(schoolsSet[position])

    override fun getItemCount(): Int = schoolsSet.size

}