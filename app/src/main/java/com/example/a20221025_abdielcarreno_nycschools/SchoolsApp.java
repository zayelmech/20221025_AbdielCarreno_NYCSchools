package com.example.a20221025_abdielcarreno_nycschools;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class SchoolsApp extends Application{

}
