package com.example.a20221025_abdielcarreno_nycschools.utils

sealed class UIState {
    object LOADING : UIState()
    data class SUCCESS<T>(val response: T): UIState()
    data class ERROR(val error: Throwable): UIState()
}