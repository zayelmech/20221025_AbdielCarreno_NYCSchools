package com.example.a20221025_abdielcarreno_nycschools.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20221025_abdielcarreno_nycschools.R
import com.example.a20221025_abdielcarreno_nycschools.adapter.SchoolsAdapter
import com.example.a20221025_abdielcarreno_nycschools.databinding.FragmentSchoolListBinding
import com.example.a20221025_abdielcarreno_nycschools.model.Schools
import com.example.a20221025_abdielcarreno_nycschools.utils.UIState
import com.example.a20221025_abdielcarreno_nycschools.viewmodel.SchoolViewModel


class SchoolFragment : Fragment() {

   val schoolsViewModel by lazy {
       ViewModelProvider(requireActivity())[SchoolViewModel::class.java]
   }

    private val binding by lazy {
        FragmentSchoolListBinding.inflate(layoutInflater)
    }
    private val recyclerViewAdapter by lazy {
        SchoolsAdapter(::showDetails) //init adapter by lazy
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // schoolsViewModel =  ViewModelProvider(this)[SchoolViewModel::class.java]

        // Setting the adapter for the recyclerview that will allow me to set and update my list once the data is ready
        binding.schoolsList.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = recyclerViewAdapter
        }
        schoolsViewModel.schools.observe(viewLifecycleOwner) { state ->
            when (state) {
                is UIState.LOADING -> {
                    Log.d("CLASS::${javaClass.simpleName} MESSAGE ->", "loading")
                    binding.progressBar.visibility = View.VISIBLE
                }
                is UIState.SUCCESS<*> -> {
                    (state as UIState.SUCCESS<List<Schools>>).response
                    binding.progressBar.visibility = View.GONE

                    state.response.let { schoolList ->
                        recyclerViewAdapter.setData(schoolList)

                    }

                    Log.d("CLASS::${javaClass.simpleName} MESSAGE ->", "success")
                }

                is UIState.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Log.d("CLASS::${javaClass.simpleName} MESSAGE ->", "error")
                }
            }

        }

        schoolsViewModel.getSchoolsList()



        binding.searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                Log.d("CLASS::${javaClass.simpleName} MESSAGE ->", "Cadena : -->$p0")
                recyclerViewAdapter.setData(schoolsViewModel.searchSchool(p0!!))
                return false
            }
        })

        return binding.root
    }

    private fun showDetails(dbn: String) {
        Log.d("CLASS::${javaClass.simpleName} MESSAGE ->", dbn)
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, DetailsFragment.newInstance(dbn))
            .addToBackStack(null)
            .commit()

    }

}